import {
  AppBar,
  MenuItem,
  Typography,
  Menu,
  Toolbar,
  Box,
  IconButton,
  Button,
} from "@mui/material";
import React from "react";
import MenuIcon from "@mui/icons-material/Menu";
import { Link } from "react-router-dom";
import "./WeddingSiteNavbar.css";

const pages = [
  { name: "Főoldal", route: "/" },
  // { name: "Program", route: "/timeline" },
  // { name: "Kérdések és válaszok", route: "/QandA" }
];

export default function WeddingSiteNavbar() {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const navs = pages.map((page) => {
    return (
      <MenuItem key={page.name}>
        <Link to={page.route} onClick={handleClose} className="Navbar-link">
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            {page.name}
          </Typography>
        </Link>
      </MenuItem>
    );
  });

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar className="Navbar-appbar">
          <IconButton
            className="Navbar-menu-button"
            id="basic-button"
            aria-controls={open ? "basic-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
            onClick={handleClick}
          >
            Menü
          </IconButton>
          <Menu
            id="basic-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            MenuListProps={{
              "aria-labelledby": "basic-button",
            }}
          >
            {navs}
          </Menu>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
