import * as React from "react";
import Timeline from "@mui/lab/Timeline";
import TimelineItem from "@mui/lab/TimelineItem";
import TimelineSeparator from "@mui/lab/TimelineSeparator";
import TimelineConnector from "@mui/lab/TimelineConnector";
import TimelineContent from "@mui/lab/TimelineContent";
import TimelineOppositeContent from "@mui/lab/TimelineOppositeContent";
import TimelineDot from "@mui/lab/TimelineDot";
import ChurchIcon from "@mui/icons-material/Church";
import PianoIcon from "@mui/icons-material/Piano";
import CakeIcon from "@mui/icons-material/Cake";
import RedeemIcon from "@mui/icons-material/Redeem";
import FastfoodIcon from "@mui/icons-material/Fastfood";
import LinkedCameraIcon from "@mui/icons-material/LinkedCamera";
import RestaurantMenuIcon from "@mui/icons-material/RestaurantMenu";
import DryCleaningIcon from "@mui/icons-material/DryCleaning";
import WomanIcon from "@mui/icons-material/Woman";
import Typography from "@mui/material/Typography";
import { Button, Container } from "@mui/material";
import "./Timeline.css";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import RestaurantIcon from "@mui/icons-material/Restaurant";

const timeline_priv = [
  {
    hour: "17:30",
    name: "Gratulációk",
    sub_text: "Because you are the Happiest Ones",
    icon: MailOutlineIcon,
  },
  {
    hour: "18:30",
    name: "Vacsora",
    sub_text: "Because you need strength",
    icon: RestaurantMenuIcon,
  },
  {
    hour: "20:30",
    name: "Nyitótánc",
    sub_text: "Because you can dance",
    icon: PianoIcon,
  },
  {
    hour: "21:00",
    name: "Meglepi",
    sub_text: "Because you love surprises",
    icon: RedeemIcon,
  },
  {
    hour: "22:00",
    name: "Torta",
    sub_text: "Because you still have space left",
    icon: CakeIcon,
  },
  {
    hour: "22:30",
    name: "Menyasszonytánc",
    sub_text: "Because you still have power",
    icon: WomanIcon,
  },
  {
    hour: "24:00",
    name: "Éjféli Menü",
    sub_text: "Because you're hungry again",
    icon: RestaurantIcon,
  },
];

const timeline_pub = [
  {
    hour: "12:00",
    name: "Készülődés",
    sub_text: "Because I want to be beautiful",
    icon: DryCleaningIcon,
  },
  {
    hour: "14:00",
    name: "Templomi szertartás",
    sub_text: "Because I need You",
    icon: ChurchIcon,
  },
  {
    hour: "15:30",
    name: "Állófogadás",
    sub_text: "Pogácsa & Pálinka",
    icon: FastfoodIcon,
  },
  {
    hour: "16:30",
    name: "Fotózás",
    sub_text: "It's all amazing",
    icon: LinkedCameraIcon,
  },
];

export default function CustomizedTimeline() {
  let timeline = timeline_pub;
  if (
    window.location.host === "hazasodik.gretibence.hu" ||
    window.location.host === "localhost:3000"
  ) {
    timeline = timeline.concat(timeline_priv);
  }

  const timelinecomp = timeline.map((element) => {
    return (
      <TimelineItem className="TimelineItem" key={element.name}>
        <TimelineOppositeContent sx={{ m: "auto 0" }} align="right">
          {element.hour}
        </TimelineOppositeContent>
        <TimelineSeparator>
          <TimelineConnector sx={{ bgcolor: "info.main" }} />
          <TimelineDot color="grey">
            <element.icon color="info" />
          </TimelineDot>
          <TimelineConnector sx={{ bgcolor: "info.main" }} />
        </TimelineSeparator>
        <TimelineContent sx={{ py: "12px", px: 2 }}>
          <Typography
            variant="inherit"
            align="center"
            className="Timeline-content"
          >
            {element.name}
          </Typography>
          <Typography
            variant="inherit"
            align="center"
            className="Timeline-content-sub"
          >
            {element.sub_text}
          </Typography>
        </TimelineContent>
      </TimelineItem>
    );
  });

  return (
    <div className="Timeline">
      <Timeline>
        <Container maxWidth="lg">{timelinecomp}</Container>
        <Container>
          {" "}
          <Typography className="Timeline-note" variant="caption">
            A programváltoztatás jogát fenntartjuk! :)
          </Typography>
        </Container>
      </Timeline>
    </div>
  );
}
