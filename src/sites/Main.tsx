import React from "react";
import "./Main.css";
import Button from "@mui/material/Button";
import CountdownTimer from "../components/CountDownTimer";

function Main() {
  let link = "https://www.flickr.com/gp/198529425@N04/21z4Y0a71i";
  let link_2 = "https://www.flickr.com/gp/198529425@N04/8Z0q62e1sj";
  let link_3 = "https://www.flickr.com/gp/198529425@N04/P48DjG95DQ";

  let link_4 = "https://www.flickr.com/gp/198529425@N04/77w29jHH25";

  const links = [
    { l: link, name: "Nászmise" },
    { l: link_2, name: "Csoportképek" },
    { l: link_3, name: "Légi felvételek" },
  ];

  if (
    window.location.host === "hazasodik.gretibence.hu" ||
    window.location.host === "localhost:3000"
  ) {
    links.push({ l: link_4, name: "Lagzi" });
  }

  const linksComp = links.map((element) => {
    return (
      <div className="Main-button">
        <Button variant="text">
          <a href={element.l} className="Main-link" target="_blank">
            {element.name}
          </a>
        </Button>
      </div>
    );
  });

  return (
    <div className="Main">
      <header className="Main-header">
        <p className="Main-title">Gréti & Bence</p>
        <p className="Main-subtitle">Köszönjük, hogy velünk ünnepeltél!</p>
        {linksComp}
      </header>
      {/* <CountdownTimer /> */}
    </div>
  );
}

export default Main;
