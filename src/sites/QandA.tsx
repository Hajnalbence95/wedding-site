import * as React from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import "./QandA.css";

const qAndA_pub = [
  {
    key: 1,
    Q: "Hol is lesz az esküvőtök pontosan?",
    A: (
      <p>
        Az esküvői szertartás helyszíne a jászkarajenei Nagyboldogasszony Római
        Katolikus templom, amelyet ezen a{" "}
        <a
          href="https://www.google.com/maps/place/J%C3%A1szkarajen%C5%91i+Nagyboldogasszony+templom/@47.0542586,20.064601,17.45z/data=!4m5!3m4!1s0x47415daa3cda6ba3:0xf3813c47013eeb6a!8m2!3d47.054048!4d20.0664711"
          target="_blank"
          rel="noreferrer"
          className="QandA-ref"
        >
          linken
        </a>{" "}
        találtok!
      </p>
    ),
  },
  {
    key: 5,
    Q: "És hol lesz az állófogadás?",
    A: (
      <p>
        Az állófogadást a templomkertben tervezzük tartani. Rossz idő esetén
        pedig... Reméljük jó idő lesz. :)
      </p>
    ),
  },
  {
    key: 4,
    Q: "Milyen ajándéknak örülnétek a legjobban?",
    A: (
      <p>
        Leginkább annak örülünk, ha velünk tartotok majd ezen a csodaszép napon,
        és jól érzitek majd magatokat! Ha ezen felül is készülnétek valamivel,
        mivel a közös életünket egy viszonylag szerény kis lakásban fogjuk
        elkezdeni, a minél appróbb, "borítéknyi" ajándékokat vesszük a
        legszívesebben.
      </p>
    ),
  },
  {
    key: 7,
    Q: "Mi lesz az esküvő színe?",
    A: (
      <p>
        Az esküvő színe (ahogy talán már erről a honlapról is kiderült) a
        világoskék lesz. Azért pont ezt a színt választottuk, mert ez Szűz Mária
        színe; az Ő oltalmába ajánljuk a házasságunkat.
      </p>
    ),
  },
  {
    key: 8,
    Q: "Tömegközlekedéssel indulunk Budapestről. Milyen útvonalat javasoltok? Hogyan a leggyorsabb megközelíteni Jászkarajenőt? ",
    A: (
      <p>
        A Google Maps ajánlása alapján, ha{" "}
        <a
          href="https://www.google.com/maps/dir/Budapest,+Nyugati+p%C3%A1lyaudvar+M/J%C3%A1szkarajen%C5%91,+2746/@47.2773775,19.3411785,10z/am=t/data=!4m18!4m17!1m5!1m1!1s0x4741dc0d3deed89b:0xe20397571e385f25!2m2!1d19.0571448!2d47.5098407!1m5!1m1!1s0x47415d9413b36605:0x400c4290c1e31b0!2m2!1d20.0653166!2d47.0545025!2m3!6e1!7e2!8j1684591200!3e3"
          target="_blank"
          rel="noreferrer"
          className="QandA-ref"
        >
          ezt az útvonalat
        </a>{" "}
        választjátok, jó eséllyel időben megérkeztek a nászmisére.
        <ul>
          <li>10:28 Budapest Nyugati Pályaudvar (vonat)</li>
          <li>11:45 Szolnok Pályaudvar—átszállás 20 perc</li>
          <li>12:05 Szolnok Jubileum tér (busz)</li>
          <li>13:00 Jászkarajenő Művelődési Ház</li>
        </ul>
      </p>
    ),
  },
  {
    key: 8,
    Q: "A regisztrációnál jeleztük, hogy hozunk süteményt. Van bármi kérésetek ezzel kapcsolatban?",
    A: (
      <p>
        Elsősorban szeretnénk megerősíteni, hogy, ha lehet, édessüteményt
        hozzatok, mivel a sós harapnivalóról mi gondosodunk. Célszerű ezekkel a
        tálcákkal már tálalható formában (felvágva, elrendezve) érkezni, ugyanis
        ilyen jellegű előkészületekre a helyszínen nem tudunk lehetőséget
        biztosítani. Végül azt is vegyétek figyelembe, hogy a sütemények hűtését
        sem tudjuk vállalni. A helyszínen látni fogjátok, hogy hol tudjátok a
        tálcákat elhelyezni.
      </p>
    ),
  },
];

const qAndA_priv = [
  {
    key: 2,
    Q: "És a lagzi hol lesz?",
    A: (
      <p>
        Hát az meg pont a templommal szemben, a{" "}
        <a
          href="https://www.google.com/maps/place/J%C3%A1szkarajen%C5%91i+Pet%C5%91fi+M%C5%B1vel%C5%91d%C3%A9si+H%C3%A1z+%C3%A9s+K%C3%B6nyvt%C3%A1r/@47.0535737,20.0668274,19.38z/data=!4m5!3m4!1s0x47415daa2504b55b:0xfba2e8674b7821ab!8m2!3d47.0534435!4d20.0666336"
          target="_blank"
          rel="noreferrer"
          className="QandA-ref"
        >
          Művelődési Házban
        </a>{" "}
        lesz ! :)
      </p>
    ),
  },
  {
    key: 3,
    Q: "Ha időközben kiderül, hogy mégsem tudok veletek ünnepelni, de már jelentkeztem, mit tegyek?",
    A: (
      <p>
        Reméljük, hogy ilyen nem fog előfordulni, de ha mégis, akkor kérünk,
        hogy mihamarabb keressetek fel bennünket bármilyen formában, hogy ennek
        megfelelően tudjunk készülni.
      </p>
    ),
  },
  {
    key: 9,
    Q: "Tudtok végül transzfert biztosítani a lakodalom és a szállás között, vagy úgy készüljünk, hogy a sofőröknek ez egy teljesen józan buli lesz?",
    A: (
      <p>
        Örömmel jelezzük felétek, hogy igen, lesz transzfer a tiszakécskei
        szállás és a Művelődési Ház között. Előreláthatólag a kisbusz az alábbi
        menetrend szerint fog közlekedni:
        <ul>
          <li>17:00: Tiszakécske -{">"} Jászkarajenő</li>
          <li>17:45: Tiszakécske -{">"} Jászkarajenő</li>
          <li>22:00: Jászkarajenő -{">"} Tiszakécske</li>
          <li>24:00: Jászkarajenő -{">"} Tiszakécske</li>
          <li>02:00: Jászkarajenő -{">"} Tiszakécske</li>
        </ul>
        így a fotózás és állófogadás alatt be tudtok jelentkezni és le tudtok
        parktolni a Parton Hotelnél, a visszajutást pedig a kisbusz fogja
        segíteni.
      </p>
    ),
  },
];

const qAndA_pub_only = [
  {
    key: 6,
    Q: "Miért kell regisztrálnom az állófogadásra? ",
    A: (
      <p>
        <s>A regisztrálók között értékes nyere...</s> Szeretnénk előre tudni,
        mennyi sütivel, üdítővel, pohárral számoljunk körülbelül, illetve a
        fotózás miatt is fontos a regisztráció, mivel előre megírt fotózási
        menetrend lesz. Aki regisztrál, ebbe a menetrendbe is bekerül majd.
      </p>
    ),
  },
];

let qAndA = qAndA_pub;

if (
  window.location.host === "hazasodik.gretibence.hu" ||
  window.location.host === "localhost:3000"
) {
  qAndA = qAndA.concat(qAndA_priv);
} else {
  qAndA = qAndA.concat(qAndA_pub_only);
}

const qAndAComp = qAndA.map((element) => {
  return (
    <Accordion key={element.key}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography component="span" className="QandA-question">
          {element.Q}
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Typography component="span" className="QandA-answer">
          {element.A}
        </Typography>
      </AccordionDetails>
    </Accordion>
  );
});

export default function SimpleAccordion() {
  return <div>{qAndAComp}</div>;
}
