import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import Main from "./sites/Main";
import reportWebVitals from "./reportWebVitals";
import WeddingSiteNavbar from "./sites/Navbar";

import { HashRouter as Router, Route, Routes } from "react-router-dom";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

export default function WeddingSiteRouter() {
  return (
    <Router>
      {/* <WeddingSiteNavbar /> */}
      <Routes>
        <Route path="/" element={<Main />} />
        {/* <Route path="/timeline" element={<CustomizedTimeline />} />
        <Route path="/QandA" element={<SimpleAccordion />} /> */}
      </Routes>
    </Router>
  );
}

root.render(
  <React.StrictMode>
    <WeddingSiteRouter />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
